const { createBookfactory, getBooksFactory, getBookByIdFactory, deleteBookByIdFactory, updateBookByIdFactory } = require("./factory");

module.exports.createBook = (req, res, next) => {
    return createBookfactory(req?.body)
        .then((data) => {
            res.status(200).json(data);
        })
        .catch(next);
};

module.exports.getBooks = (req, res, next) => {
    return getBooksFactory()
        .then((data) => {
            res.status(201).json(data);
        })
        .catch(next);
};

module.exports.getBookById = (req, res, next) => {
    return getBookByIdFactory(req?.params?.id)
        .then((data) => {
            res.status(201).json(data);
        })
        .catch(next);
}

module.exports.updateBookById = (req, res, next) => {
    return updateBookByIdFactory(req?.params?.id, req?.body)
        .then((data) => {
            res.status(201).json(data);
        })
        .catch(next);
}

module.exports.deleteBookById = (req, res, next) => {
    return deleteBookByIdFactory(req?.params?.id)
        .then((data) => {
            res.status(201).json(data);
        })
        .catch(next);
} 