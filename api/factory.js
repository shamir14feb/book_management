const Books = require("./model");

module.exports.createBookfactory = async (body) => {
    if (!body?.title) {
        throw new Error("Title is required");
    }

    if (typeof body?.title != "string") {
        throw new Error("Title should be string")
    }


    if (!body?.author) {
        throw new Error("Author is required");
    }

    if (typeof body?.author != "string") {
        throw new Error("author should be in string")
    }

    if (typeof body?.summary != "undefined" && typeof body?.summary != "string") {
        throw new Error("summary should be in string")
    }

    return await Books
        .create(body);
}

module.exports.getBooksFactory = async () => {
    return await Books
        .find();
}

module.exports.getBookByIdFactory = async (id) => {
    return await Books
        .findOne({
            _id: id
        });
}

module.exports.updateBookByIdFactory = async (id, body) => {
    if (!body?.title) {
        throw new Error("Title is required");
    }

    if (typeof body?.title != "string") {
        throw new Error("Title should be string")
    }


    if (!body?.author) {
        throw new Error("Author is required");
    }

    if (typeof body?.author != "string") {
        throw new Error("author should be in string")
    }

    if (typeof body?.summary != "undefined" && typeof body?.summary != "string") {
        throw new Error("summary should be in string")
    }
    
    return await Books
        .updateOne({
            _id: id
        }, {
            $set: body
        });
}

module.exports.deleteBookByIdFactory = async (id) => {
    return await Books
        .findByIdAndDelete(id)
}