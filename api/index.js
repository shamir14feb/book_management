const { Router } = require("express");
const { createBook, getBooks, getBookById, updateBookById, deleteBookById } = require("./controller");

const router = Router();

router.post("/", createBook);

router.get("/", getBooks);

router.get("/:id", getBookById);

router.put("/:id", updateBookById);

router.delete("/:id", deleteBookById);

module.exports = router;