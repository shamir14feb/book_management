const { Schema, model } = require("mongoose");

const bookSchema = new Schema(
    {
        title: {
            type: String,
            required: true
        },
        author: {
            type: String,
            required: true
        },
        summary: {
            type: String
        }
    }
);

module.exports = model("book", bookSchema);