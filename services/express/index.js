const bodyParser = require("body-parser");
const express = require("express");

module.exports.expressService = () => {
    const app = express();

    app.use(bodyParser.urlencoded({
        extended: false,
    }));

    app.use(bodyParser.json());

    return app;
}