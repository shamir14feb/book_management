const mongoose = require("mongoose");

mongoose.connection.on("error", (err) => {
    console.error("MongoDB Connection Error: " + err);
    process.exit(-1);
});

mongoose.connection.once("open", () => {
    console.log("MongoDB connection success");
});

module.exports = mongoose;