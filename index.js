require("dotenv").config();
const mongoose = require("./services/mongoose");
const { expressService } = require("./services/express");
const api = require("./api");

const port = process.env.PORT;
const mongodbUri = process.env.MONGODB_URI;

const app = expressService();

mongoose
    .connect(mongodbUri);

app.use("/api", api);

app.use((error, request, response, next) => {
    console.log(error);
    next();
});

app.listen(port, () => {
    console.log("Server is running on port: ", port);
});