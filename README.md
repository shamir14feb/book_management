# Book_Management



## Getting started

1. Download or Forks the repo
2. use this command on terminal to install all dependencies `npm install`
3. after all dependencies has been installed create `.env` file on root scope (where package.json file exists)
4. In `.env` file you have to define `PORT` and `MONGODB_URI`
5. After every thing is setup you can start a server by `npm start` or `npm run dev` on terminal

## API endpoints and their usage

1. POST `/api` is to and the new book (Title, Author, summary)
2. GET `/api` is to view a list of all books
3. GET `/api/:id` is to view details of specific details of a specific book by its id
4. PUT `/api/:id` is to update details of a book by its id
5. DELETE `/api/:id` is to delete the book by its id